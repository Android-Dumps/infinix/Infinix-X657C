#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:a6eefd837b51e13add5011d2eeed1b3907bff7cc; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:0b29bb5053bcb045a5401a03515e4b3945a8f35c \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:a6eefd837b51e13add5011d2eeed1b3907bff7cc && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
